<?php

  function tag6_file() {
    
    $content = "<div id=\"christmasgame\">
    <section id=\"headerSection\">
    <h2>Das Schie-LE</h2>
    <h1>AdventsSpiel</h2>
  </section>
  <section id=\"gameSection\">
    <canvas id=\"mainCanvas\" class=\"hasShadow\" width=\"600\" height=\"450\" ondrop=\"droppedPresent(event)\" ondragover=\"presentOverCanvas(event)\">
      Your browser does not support the HTML5 canvas tag.
    </canvas>
  </section>
  <section id=\"footerSection\">
    <div id=\"info\">
      <div id=\"messagebox\" class=\"infoItems\"><p id=\"notification\"></p></div>
      <div id=\"legalInfo\"><p>Quelle: <a href=\"https://soundcloud.com/loicmusique/8bit-christmas\">Hintergrundmusik</a></p></div>
    </div>
  </section>
  </div>";

    return $content;
    
  }

?>
