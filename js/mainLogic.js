// map information
var grid = { gridH:30, gridW: 40, tileSizeH:0, tileSizeW:0, gridContent:[] };

var gridEmptyID = 0;
var gridHouseID = 1;
var gridTreeOneID = 2;
var gridTreeTwoID = 3;
var gridTreeThreeID = 4;
var gridSpawnPointID = 5;
var gridPlayerID = 6;

var scaleHouse = 2;
var scaleSpawnPoint = 2;

var houseCount = 15;
var treeCount = 80;

// asset data
var mainCanvas;
var ctx;
var imagePaths = ['png/treeOne.png', 'png/treeTwo.png', 'png/treeThree.png', 'png/roof.png', 'png/santa.png'];
var imageTextures = [];
var imagesLoaded = 0;

// game data
var roundTimeInSeconds = 60;
var activeTimer;
var houseList = [];

var activeGame = {
	player : {
		positionX : 0,
		positionY : 0,
		moveUp : function() {
			if(isAVisitablePlace(activeGame.player.positionX, activeGame.player.positionY - 1)) {
				activeGame.player.positionY--;
				activeGame.player.enteredNewGridField(activeGame.player.positionX, activeGame.player.positionY + 1);
			}
			
		},
		moveLeft : function() {
			if(isAVisitablePlace(activeGame.player.positionX - 1, activeGame.player.positionY)) {
				activeGame.player.positionX--;
				activeGame.player.enteredNewGridField(activeGame.player.positionX + 1, activeGame.player.positionY);
			}
			
		},
		moveRight : function() {
			if(isAVisitablePlace(activeGame.player.positionX + 1, activeGame.player.positionY)) {
				activeGame.player.positionX++;
				activeGame.player.enteredNewGridField(activeGame.player.positionX - 1, activeGame.player.positionY);
			}
			
		},
		moveDown : function() {
			if(isAVisitablePlace(activeGame.player.positionX, activeGame.player.positionY + 1)) {
				activeGame.player.positionY++;
				activeGame.player.enteredNewGridField(activeGame.player.positionX, activeGame.player.positionY - 1);
			}
			
		},
		enteredNewGridField : function(oldGridX, oldGridY) {
			var fieldID = grid.gridContent[activeGame.player.positionX][activeGame.player.positionY].contentID;

			switch(fieldID) {
				case gridHouseID:
					var oldFieldID = grid.gridContent[oldGridX][oldGridY].contentID;

					// make sure that you can't get more then one point
					if(oldFieldID != gridHouseID) {
						activeGame.destinyCoords.forEach(function(coords) {

							// test for the right coordinates
							if(coords.gridX == activeGame.player.positionX && coords.gridY == activeGame.player.positionY) {
								activeGame.player.reachedHouse();
								return;
							}
						});
					}
				break;
				default:
			}
		},
		reachedHouse : function() {
			activeGame.points += 10;
			activeGame.coinAudio.play();
			activeGame.setNextHouseAsDestiny();
		}
	},
	secondsLeft : roundTimeInSeconds,
	points : 0,
	isActive : false,
	destinyHouseID : 0,
	backgroundAudio : undefined,
	coinAudio : undefined,
	checkAudio : undefined,
	isAudioActive : true,
	destinyCoords : [],
	startTimer : function() {
		activeTimer = window.setInterval("activeGame.countDown()", 1000);
	},
	countDown : function() {
		activeGame.secondsLeft--;

		if(activeGame.secondsLeft <= 0) {
			activeGame.isActive = false;
		}
	},
	start : function() {
		showNotification("Spiel lädt");

		houseList = [];
		loadLogic();

		// reset game
		activeGame.setNextHouseAsDestiny();
		activeGame.secondsLeft = roundTimeInSeconds;
		
		// load music
		activeGame.backgroundAudio = new Audio("http://www.hofgemeinde-leipzig.de/fileadmin/package/hofgemeinde-leipzig.de/scripts/tag06/sounds/background.mp3");
		activeGame.backgroundAudio.loop = false;
		activeGame.backgroundAudio.muted = !activeGame.isAudioActive;
		activeGame.backgroundAudio.load();

		// load coin sound
		activeGame.coinAudio = new Audio("http://www.hofgemeinde-leipzig.de/fileadmin/package/hofgemeinde-leipzig.de/scripts/tag06/sounds/coin.mp3");
		activeGame.coinAudio.loop = false;
		activeGame.coinAudio.muted = !activeGame.isAudioActive;
		activeGame.coinAudio.load();

		activeGame.points = 0;
		// wait for music to load
		activeGame.checkAudio = window.setInterval( function() { activeGame.startAfterLoading() }, 1000);
	},
	stop : function() {
		window.clearInterval(activeTimer);
		showEndMenu();
	},
	startAfterLoading : function() {
		if (activeGame.backgroundAudio.readyState === 4 && activeGame.coinAudio.readyState === 4) {
			window.clearInterval(activeGame.checkAudio);
			activeGame.isActive = true;
			activeGame.startTimer();
			activeGame.backgroundAudio.play();
			showNotification("Viel Spaß!");
			gameLoop();
		}
	},
	setNextHouseAsDestiny : function() {
		do {
			var randomHouseID = Math.round((Math.random() * houseList.length) - 0.5);
		} while(randomHouseID == activeGame.destinyHouseID);

		var newHouse = houseList[randomHouseID];
		var oldHouse = houseList[activeGame.destinyHouseID];
		activeGame.destinyHouseID = randomHouseID;

		// clear old destiny coordinations
		activeGame.destinyCoords = [];

		// set new destiny
		for (var x = 0; x < scaleHouse; x++) {
			for (var y = 0; y < scaleHouse; y++) {
				// remove old destiny
				grid.gridContent[oldHouse.gridX + x][oldHouse.gridY + y].isDestiny = false;

				// set new destiny
				grid.gridContent[newHouse.gridX + x][newHouse.gridY + y].isDestiny = true;
				var coords = { gridX : newHouse.gridX + x, gridY : newHouse.gridY + y };
				activeGame.destinyCoords.push(coords);
			}
		}
	}
}

// start method on startup
window.onload = function() {
	mainCanvas = document.getElementById("mainCanvas");
	window.addEventListener("keydown", doOnKeyDown, true);

	initGame();
}

function clearCanvas() {
	mainCanvas.width = mainCanvas.width;
}

function showNotification(message) {
	var notificationElem = document.getElementById('notification');
	notificationElem.innerHTML = message;
}

function initGame() {
	showNotification("Laden...");

	ctx = mainCanvas.getContext("2d");
	ctx.imageSmoothingEnabled = false;

	loadImagesAndLogic(imagePaths);
}

function loadImagesAndLogic(paths) {
	var loadedImages = 0;

	paths.forEach(function(path, id) {
		var image = new Image;
		image.src = "http://www.hofgemeinde-leipzig.de/fileadmin/package/hofgemeinde-leipzig.de/scripts/tag06/images/" + path;
		image.onload = function() {
			imageTextures[id] = image;
			loadedImages++;

			// load all textures before starting the game
			if(loadedImages == paths.length) {
				// start the game
				showNotification("Viel Spaß!");
				showStartMenu();
			}
		}
		
	});
}

function showStartMenu() {
	ctx.fillStyle = "#D9D9D9";
	ctx.fillRect(0, 0, mainCanvas.width, mainCanvas.height);

	// draw text
	ctx.fillStyle = "#000000";
	ctx.font = "36pt Yanone Kaffeesatz";
	ctx.fillText("Willkommen beim Adventsspiel!", 30, 60);

	ctx.font = "15pt Yanone Kaffeesatz";
	ctx.fillText("Das Prinzip ist simpel: hilf dem alten Mann mit Rauschebart und rotem Mantel.", 30, 120);
	ctx.fillText("Er muss möglichst schnell die gelb markierten Häuser erreichen", 30, 150);
	ctx.fillText("und kleine bunte Päckchen los werden.", 30, 180);
	ctx.fillText("Aber er hat nur 60 Sekunden Zeit!", 30, 210);
	ctx.fillText("Steuerung: W, A, S, D", 30, 270);
	ctx.fillText("Musik an/aus: M", 30, 300);

	ctx.font = "36pt Yanone Kaffeesatz";
	ctx.fillStyle = "#000000";
	ctx.fillText("Drücke Enter zum Starten!", 30, mainCanvas.height - 30);
}

function showEndMenu() {
	clearCanvas();

	// draw background
	ctx.fillStyle = "#D9D9D9";
	ctx.fillRect(0, 0, mainCanvas.width, mainCanvas.height);

	// draw text
	ctx.fillStyle = "#000000";
	ctx.font = "36pt Yanone Kaffeesatz";
	ctx.fillText("Du hast " + activeGame.points + " Punkte erreicht!", 30, 60);

	var textOffset = 90;
	var lineHeight = 50;

	ctx.font = "15pt Yanone Kaffeesatz";
	ctx.fillText("Glücklicherweise besteht Weihnachten nicht nur aus bunten Geschenken, schriller Musik", 30, textOffset + lineHeight);
	ctx.fillText("und alten Männern mit roten Mützen!", 30, textOffset + lineHeight * 2);
	ctx.fillText("Die Schie-LE-Jugend wünscht Dir eine gesegnete Adventszeit.", 30, textOffset + lineHeight * 4);

	ctx.font = "24pt Yanone Kaffeesatz";
	ctx.fillText("Mit Enter ein neues Spiel starten!", 30, mainCanvas.height - 80);
}

function loadLogic() {
	initEmptyMap();
	addRandomElementsToMap(treeCount, houseCount);
	findSpawnAndSetPlayer();
}


function initEmptyMap() {
	// one line with columns
	grid.gridContent = [];

	for (var i = 0; i < grid.gridW; i++) {
		grid.gridContent[i] = [];

		// every row gets initialized
		for (var k = 0; k < grid.gridH; k++) {
			grid.gridContent[i][k] = { contentID: gridEmptyID, needsRedraw: false, isDestiny: false };
		}
	}

	// calculate tilesize
	grid.tileSizeH = (mainCanvas.height - (mainCanvas.height % grid.gridH)) / grid.gridH;
	grid.tileSizeW = (mainCanvas.width - (mainCanvas.width % grid.gridW)) / grid.gridW;
}

function drawMap() {
	clearCanvas();

	// draw background
	ctx.fillStyle = "#F7F7F7";
	ctx.fillRect(0, 0, mainCanvas.width, mainCanvas.height);

	// draw special fields
	for (var i = 0; i < grid.gridW; i++) {
		for (var k = 0; k < grid.gridH; k++) {

			if(grid.gridContent[i][k].needsRedraw) {
				switch(grid.gridContent[i][k].contentID) {
					case gridTreeOneID:
						ctx.drawImage(imageTextures[0], grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW, grid.tileSizeH);
					break;
					case gridTreeTwoID:
						ctx.drawImage(imageTextures[1], grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW, grid.tileSizeH);
					break;
					case gridTreeThreeID:
						ctx.drawImage(imageTextures[2], grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW, grid.tileSizeH);
					break;
					case gridHouseID:
						ctx.drawImage(imageTextures[3], grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW * scaleHouse, grid.tileSizeH * scaleHouse);

						// draw houses which are more then one grid unit large
						for (var x = 0; x < scaleHouse; x++) {
							for (var y = 0; y < scaleHouse; y++) {
								var xToSkip = i + x;
								var yToSkip = k + y;

								if(xToSkip < grid.gridW && yToSkip < grid.gridH) {
									grid.gridContent[xToSkip][yToSkip].needsRedraw = false;
								}
							}
						}

						// make sure that the base field for the texture gets updated!
						grid.gridContent[i][k].needsRedraw = true;
					break;
					case gridSpawnPointID:
						ctx.fillStyle = "#D9D9D9";
						ctx.fillRect(grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW, grid.tileSizeH);
					break;
				}
			}

			// always draw destiny
			if(grid.gridContent[i][k].isDestiny) {

				ctx.fillStyle = "rgba(230, 230, 0, 0.5)";
				ctx.fillRect(grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW, grid.tileSizeH);
			}

			// always draw player
			if(i == activeGame.player.positionX) {
				if(k == activeGame.player.positionY) {
					ctx.drawImage(imageTextures[4], grid.tileSizeW * i, grid.tileSizeH * k, grid.tileSizeW , grid.tileSizeH);
				}
			}
		}
	}

	// draw HUD
	ctx.font = "20pt Yanone Kaffeesatz";
	ctx.fillStyle = "#000000";

	// draw seconds left
	ctx.fillText(activeGame.secondsLeft + " s", 30, mainCanvas.height - 30);
	// draw points
	ctx.fillText(activeGame.points + " P", 30, mainCanvas.height - 60);
}

function addRandomElementsToMap(treeCount, houseCount) {
	addElementsToMap(1, scaleSpawnPoint, gridSpawnPointID);

	addElementsToMap(houseCount, scaleHouse, gridHouseID);

	addElementsToMap(treeCount, 1, gridTreeOneID);
	addElementsToMap(treeCount, 1, gridTreeTwoID);
	addElementsToMap(treeCount, 1, gridTreeThreeID);
}

function addElementsToMap(count, scale, ID) {
	var successfullAdds = 0;

	do {
		if(addedUniqueElementToMap(scale, ID)) {
			successfullAdds++;
		}
	} while(successfullAdds < count);
}

function spaceIsFree(gridX, gridY, scale) {
	for(var x = 0; x < scale; x++) {
		for(var y = 0; y < scale; y++) {
			if(grid.gridContent[gridX + x][gridY + y].contentID != gridEmptyID) {
				return false;
			}
		}
	}

	return true;
}

function addedUniqueElementToMap(scale, ID) {
	var freeSpace = true;

	var randomX = Math.round((Math.random() * (grid.gridW - scale + 1)) - 0.5);
	var randomY = Math.round((Math.random() * (grid.gridH - scale + 1)) - 0.5);

	if(spaceIsFree(randomX, randomY, scale)) {
		for(var x = 0; x < scale; x++) {
			for(var y = 0; y < scale; y++) {
				grid.gridContent[randomX + x][randomY + y].contentID = ID;
				grid.gridContent[randomX + x][randomY + y].needsRedraw = true;
			}
		}

		// add created house to the list of houses
		if(ID == gridHouseID) {
			var houseInfo = {
				gridX : randomX,
				gridY : randomY,
			};
			houseList.push(houseInfo);
		}

		return true;
	} else {

		return false;
	}
}

function isAVisitablePlace(gridX, gridY) {
	// prevend the user before leaving the game area
	if(gridX < grid.gridW && gridY < grid.gridH) {
		if(gridX >= 0 && gridY >= 0) {
			switch(grid.gridContent[gridX][gridY].contentID ) {
				case gridEmptyID:
				case gridHouseID:
				case gridSpawnPointID:
					return true;
				default:
					return false;
			}
		}
	} else {
		return false;
	}
}

function gameLoop() {
	reqAnimFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;

    reqAnimFrame(gameLoop);

    // check if the game is still active
    if(activeGame.isActive) {
    	drawMap();
    } else {
    	activeGame.stop();
    	return;
    }
}

function findSpawnAndSetPlayer() {
	for(var x = 0; x < grid.gridW; x++) {
		for(var y = 0; y < grid.gridH; y++) {
			if(grid.gridContent[x][y].contentID == gridSpawnPointID) {
				activeGame.player.positionX = x;
				activeGame.player.positionY = y;

				break;
			}
		}
	}
}

function doOnKeyDown(e) {
	if(activeGame.isActive) {
		switch(e.keyCode) {
			case 87:
				activeGame.player.moveUp();
			break;
			case 83:
				activeGame.player.moveDown();
			break;
			case 65:
				activeGame.player.moveLeft();
			break;
			case 68:
				activeGame.player.moveRight();
			break;
			case 77:
				// turn sound on and off
				activeGame.isAudioActive = !activeGame.isAudioActive;
				activeGame.backgroundAudio.muted = !activeGame.isAudioActive;
				activeGame.coinAudio.muted = !activeGame.isAudioActive;
			break;
			default:
		}
	} else {
		switch(e.keyCode) {
			case 13:
				activeGame.start();
			break;
		}
	}
}

function presentOverCanvas(e) {

}

function droppedPresent(e) {
	alert("got a present!");
}

function draggedPresent(e) {

}